/*
 * Copyright (c) 2011 Nokia Corporation.
 */

#ifndef ACCELEROMETERFILTER_H
#define ACCELEROMETERFILTER_H

#include <QAccelerometerFilter>
#include <QVariant>

QTM_USE_NAMESPACE

class AccelerometerFilter
    : public QObject, public QAccelerometerFilter
{
    Q_OBJECT

protected:
    qreal x;
    qreal y;
    qreal z;
  
public:
    qreal last_x;
    qreal last_y;
    qreal last_z;
    AccelerometerFilter();
    bool filter(QAccelerometerReading *reading);
    void getAcceleration(qreal &x, qreal &y, qreal &z);


signals:
    void rotationChanged(const QVariant &deg);
};

#endif // ACCELEROMETERFILTER_H
